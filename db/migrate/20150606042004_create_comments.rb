class CreateComments < ActiveRecord::Migration
  def change
    create_table :comments do |t|
      t.string :post
      t.integer :id
      t.string :bodytext

      t.timestamps null: false
    end
  end
end
